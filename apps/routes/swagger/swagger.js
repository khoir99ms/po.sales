import { Router } from 'express';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from '../../configs/swagger.js';

const route = () => {
    let api = Router();

    api.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    api.use('/api/v1.0', api);
    
    return api;
}

export default route;