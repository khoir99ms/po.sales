import { Router } from 'express';

const route = () => {
    let api = Router();

    api.get('/hello', (req, res) => {
        res.json({ 
            "version": "1.0", 
            "type": "items", 
        });
    });

    return api;
}

export default route;