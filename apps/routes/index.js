import { Router } from 'express';
import items from './items';
import orders from './orders';
import tenders from './tenders';
import swagger from './swagger/swagger';

const routes = () => {
    let api = Router();

    api.get('/', (req, res) => {
        res.json({
            "version": "1.0",
            "message": "welcome",
        });
    });
    api.use('/items', items());
    api.use('/orders', orders());
    api.use('/tenders', tenders());
    
    api.use('/docs', swagger());

    return api;
}

export default routes;