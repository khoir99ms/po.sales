import config from './files/config.json';

config.mongo.uri = `mongodb://${config.mongo.host}:${config.mongo.port}/${config.mongo.database}`;

export default config;