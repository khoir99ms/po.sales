import mongoose from 'mongoose';
// make bluebird default Promise
import Promise from 'bluebird';
import config from './config';

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;
// connect to mongo db
mongoose.connect(config.mongo.uri, 
    {
        poolSize: 100,
        keepAlive: 1,
        autoReconnect: 1
    }
);
mongoose.connection.on('error', () => {
    throw new Error(`unable to connect to database: ${config.mongo.uri}`);
});

// print mongoose logs in dev env
if (config.mongo.debug) {
    mongoose.set('debug', (collection, method, query, doc) => {
        debug(`${collection}.${method}`, util.inspect(query, false, 20), doc);
    });
}

export default mongoose;