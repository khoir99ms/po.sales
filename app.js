import express from 'express';
import bodyParser from 'body-parser';
import routes from './apps/routes';
import config from './apps/configs/config';
import mongo from './apps/configs/storage.mongo';

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', (req, res, next) => {
    next();
});
app.use('/api/v1.0', routes());

app.listen(config.port, () => {
    console.log(`server started on port : ${config.port}`);
});